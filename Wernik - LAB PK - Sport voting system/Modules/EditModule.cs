﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Wernik___LAB_PK___Sport_voting_system.Models
{
    public class EditModule : Nancy.NancyModule
    {
        public EditModule()
        {
            Get["/edit"] = parameter => {
                Console.WriteLine("Get[/edit]");
                var athletes = getPrincipants();
                var model = new { msg = nullMsg, athletes, edit = disable };
                return View["edit.html", model]; 
            };

            Post["/edit"] = parameter => {
                var body = this.Request.Body;
                int length = (int)body.Length;
                byte[] data = new byte[length];
                body.Read(data, 0, length);
                Console.WriteLine("Post[/edit] :: "+System.Text.Encoding.Default.GetString(data));
                var athletes = getPrincipants();
                if (data.Length > 0)
                {
                    string[] tokens = System.Text.Encoding.Default.GetString(data).Split(new[] { "&" }, StringSplitOptions.None);
                    String cmd = (tokens[0].Split(new[] { "=" }, StringSplitOptions.None))[1];
                    String id = (tokens[1].Split(new[] { "=" }, StringSplitOptions.None))[1];
                    switch (cmd)
                    {
                        case "edit":
                            {
                                //var athletes = getPrincipants();
                                var athlete = getPrincipant(id);
                                var modelEdit = new {
                                    msg = nullMsg,
                                    athletes,
                                    edit = enable,
                                    showInForm = true,
                                    Id = athlete.ElementAt(0).Id,
                                    Name = athlete.ElementAt(0).Name,
                                    Surname = athlete.ElementAt(0).Surname,
                                    Description = athlete.ElementAt(0).Description,
                                    Sport = athlete.ElementAt(0).Sport
                                };
                                return View["edit.html", modelEdit];
                            }
                            break;
                        case "save":
                            {
                                //String cmd = (tokens[0].Split(new[] { "=" }, StringSplitOptions.None))[1];
                                //String id = (tokens[1].Split(new[] { "=" }, StringSplitOptions.None))[1];
                                String Name = (tokens[2].Split(new[] { "=" }, StringSplitOptions.None))[1].
                                                Replace("+", " ").Replace("%0D%0A", "<br>").Replace("%28", "(").Replace("%29", ")").Replace("%C4%85", "ą").Replace("%C4%87", "ć").Replace("%C4%99", "ę").Replace("%C5%82", "ł").Replace("%C5%84", "ń").Replace("%C3%B3", "ó").Replace("%C5%9B", "ś").Replace("%C5%BA", "ź").Replace("%C5%BC", "ż").
                                                Replace("%C4%84", "Ą").Replace("%C4%86", "Ć").Replace("%C4%98", "Ę").Replace("%C5%BB", "Ż").Replace("%C5%B9", "Ź").Replace("%C5%81", "Ł").Replace("%C3%93", "Ó").Replace("%C5%83", "Ń").Replace("%C5%9A", "Ś").Replace("%E2%80%93","-").Replace("%2C", ",");
                                String Surname = (tokens[3].Split(new[] { "=" }, StringSplitOptions.None))[1].
                                                Replace("+", " ").Replace("%0D%0A", "<br>").Replace("%28", "(").Replace("%29", ")").Replace("%C4%85", "ą").Replace("%C4%87", "ć").Replace("%C4%99", "ę").Replace("%C5%82", "ł").Replace("%C5%84", "ń").Replace("%C3%B3", "ó").Replace("%C5%9B", "ś").Replace("%C5%BA", "ź").Replace("%C5%BC", "ż").
                                                Replace("%C4%84", "Ą").Replace("%C4%86", "Ć").Replace("%C4%98", "Ę").Replace("%C5%BB", "Ż").Replace("%C5%B9", "Ź").Replace("%C5%81", "Ł").Replace("%C3%93", "Ó").Replace("%C5%83", "Ń").Replace("%C5%9A", "Ś").Replace("%E2%80%93", "-").Replace("%2C", ",");
                                String Description = (tokens[4].Split(new[] { "=" }, StringSplitOptions.None))[1].
                                                Replace("+", " ").Replace("%0D%0A", "<br>").Replace("%28", "(").Replace("%29", ")").Replace("%C4%85", "ą").Replace("%C4%87", "ć").Replace("%C4%99", "ę").Replace("%C5%82", "ł").Replace("%C5%84", "ń").Replace("%C3%B3", "ó").Replace("%C5%9B", "ś").Replace("%C5%BA", "ź").Replace("%C5%BC", "ż").
                                                Replace("%C4%84", "Ą").Replace("%C4%86", "Ć").Replace("%C4%98", "Ę").Replace("%C5%BB", "Ż").Replace("%C5%B9", "Ź").Replace("%C5%81", "Ł").Replace("%C3%93", "Ó").Replace("%C5%83", "Ń").Replace("%C5%9A", "Ś").Replace("%E2%80%93", "-").Replace("%2C", ",");
                                String Sport = (tokens[5].Split(new[] { "=" }, StringSplitOptions.None))[1].
                                                Replace("+", " ").Replace("%0D%0A", "<br>").Replace("%28", "(").Replace("%29", ")").Replace("%C4%85", "ą").Replace("%C4%87", "ć").Replace("%C4%99", "ę").Replace("%C5%82", "ł").Replace("%C5%84", "ń").Replace("%C3%B3", "ó").Replace("%C5%9B", "ś").Replace("%C5%BA", "ź").Replace("%C5%BC", "ż").
                                                Replace("%C4%84", "Ą").Replace("%C4%86", "Ć").Replace("%C4%98", "Ę").Replace("%C5%BB", "Ż").Replace("%C5%B9", "Ź").Replace("%C5%81", "Ł").Replace("%C3%93", "Ó").Replace("%C5%83", "Ń").Replace("%C5%9A", "Ś").Replace("%E2%80%93", "-").Replace("%2C", ",");
                                SQLiteDatabase DB = new SQLiteDatabase(Assembly.GetEntryAssembly().Location, "svs.db");
                                DB.ExecuteNonQuery("UPDATE ParticipantInfo SET Name = '" + Name + "', Surname = '" + Surname + "', Description = '" + Description + "', Sport = '" + Sport + "' WHERE Id == " + id + "");
                                athletes = getPrincipants();
                                var modelSave = new { msg = successMsg, athletes, edit = disable };
                                return View["edit.html", modelSave];
                            }
                            break;
                        case "delete":
                            {
                                SQLiteDatabase DB = new SQLiteDatabase(Assembly.GetEntryAssembly().Location, "svs.db");
                                DB.ExecuteNonQuery("UPDATE ParticipantInfo SET Status = 0 WHERE Id == " + id + "");
                                athletes = getPrincipants();
                                var modelSave = new { msg = deleteMsg, athletes, edit = disable };
                                return View["edit.html", modelSave];
                            } break;
                        default:
                            {
                            }
                            break;
                    }
                }
                athletes = getPrincipants();
                var model = new { msg = nullMsg, athletes, edit = disable };
                return View["edit.html", model];
            };
        }

        List<Participant> getPrincipants()
        {
            SQLiteDatabase DB = new SQLiteDatabase(Assembly.GetEntryAssembly().Location, "svs.db");
            DataTable dataDB = DB.GetDataTable("SELECT Id, Name, Surname, Description, Sport, Status FROM ParticipantInfo WHERE Status==1");
            var athlete = Enumerable
                .Range(1, dataDB.Rows.Count)
                .Select(i => new Participant());

            int k = 0;
            int xp = 0, xn = 0;
            List<Participant> ath = athlete.ToList<Participant>();
            foreach (DataRow row in dataDB.Rows)
            {
                ath.ElementAt(k).Id = (int)row.Field<Int64>(0);
                ath.ElementAt(k).Name = row.Field<string>(1);
                ath.ElementAt(k).Surname = row.Field<string>(2);
                ath.ElementAt(k).Description = row.Field<string>(3);
                ath.ElementAt(k).Sport = row.Field<string>(4);
                Int32.TryParse(DB.ExecuteScalar("SELECT count(*) FROM Votes WHERE ParticipantId=='" + (int)row.Field<Int64>(0) + "' and Status=='1'"), out xp);
                Int32.TryParse(DB.ExecuteScalar("SELECT count(*) FROM Votes WHERE ParticipantId=='" + (int)row.Field<Int64>(0) + "' and Status=='0'"), out xn);
                ath.ElementAt(k).Points = xp - xn;
                k++;
            }
            List<Participant> sath = ath.OrderBy(o => o.Name).ToList();
            return sath;
        }

        List<Participant> getPrincipant(String id)
        {
            SQLiteDatabase DB = new SQLiteDatabase(Assembly.GetEntryAssembly().Location, "svs.db");
            DataTable dataDB = DB.GetDataTable("SELECT Id, Name, Surname, Description, Sport FROM ParticipantInfo WHERE Id=="+id+"");
            var athletes = Enumerable
                .Range(1, dataDB.Rows.Count)
                .Select(i => new Participant());

            int k = 0;
            int xp = 0, xn = 0;
            List<Participant> ath = athletes.ToList<Participant>();
            foreach (DataRow row in dataDB.Rows)
            {
                ath.ElementAt(k).Id = (int)row.Field<Int64>(0);
                ath.ElementAt(k).Name = row.Field<string>(1);
                ath.ElementAt(k).Surname = row.Field<string>(2);
                ath.ElementAt(k).Description = row.Field<string>(3);
                ath.ElementAt(k).Sport = row.Field<string>(4);
                Int32.TryParse(DB.ExecuteScalar("SELECT count(*) FROM Votes WHERE ParticipantId=='" + (int)row.Field<Int64>(0) + "' and Status=='1'"), out xp);
                Int32.TryParse(DB.ExecuteScalar("SELECT count(*) FROM Votes WHERE ParticipantId=='" + (int)row.Field<Int64>(0) + "' and Status=='0'"), out xn);
                ath.ElementAt(k).Points = xp - xn;
                k++;
            }
            return ath;
        }
        
        const String successMsg = @"
            <div class='alert alert-success' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
                <strong>Well done! </strong>
                Your changes will saved ;)
            </div>
            ";

        const String deleteMsg = @"
            <div class='alert alert-danger' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
                <strong>Well done! </strong>
                Selected participant was deleted!
            </div>
            ";
        const String nullMsg = "";
        const String enable = "";
        const String disable = "disabled";

    }
}