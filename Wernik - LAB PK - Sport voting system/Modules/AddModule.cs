﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using System.Web;
using System.Reflection;
using System.Net;

namespace Wernik___LAB_PK___Sport_voting_system.Models
{
    public class AddModule : Nancy.NancyModule
    {
        public AddModule()
        {
            Get["/add"] = parameter => {
                Console.WriteLine("Get[/add]");
                return View["add.html", new { msg = "" }];
            };
            
            Post["/add"] = parameter => {
                var body = this.Request.Body;
                int length = (int)body.Length;
                byte[] data = new byte[length];
                body.Read(data, 0, length);
                Console.WriteLine("Post[/add] :: " + System.Text.Encoding.Default.GetString(data));                
                String[] tokens = System.Text.Encoding.Default.GetString(data).Split(new[] { "&" }, StringSplitOptions.None);
                String NameP = (tokens[0].Split(new[] { "=" }, StringSplitOptions.None))[1].
                            Replace("+", " ").Replace("%0D%0A", "<br>").Replace("%28", "(").Replace("%29", ")").Replace("%C4%85", "ą").Replace("%C4%87", "ć").Replace("%C4%99", "ę").Replace("%C5%82", "ł").Replace("%C5%84", "ń").Replace("%C3%B3", "ó").Replace("%C5%9B", "ś").Replace("%C5%BA", "ź").Replace("%C5%BC", "ż").
                            Replace("%C4%84", "Ą").Replace("%C4%86", "Ć").Replace("%C4%98", "Ę").Replace("%C5%BB", "Ż").Replace("%C5%B9", "Ź").Replace("%C5%81", "Ł").Replace("%C3%93", "Ó").Replace("%C5%83", "Ń").Replace("%C5%9A", "Ś").Replace("%E2%80%93", "-").Replace("%2C", ",");
                String SurnameP = (tokens[1].Split(new[] { "=" }, StringSplitOptions.None))[1].
                            Replace("+", " ").Replace("%0D%0A", "<br>").Replace("%28", "(").Replace("%29", ")").Replace("%C4%85", "ą").Replace("%C4%87", "ć").Replace("%C4%99", "ę").Replace("%C5%82", "ł").Replace("%C5%84", "ń").Replace("%C3%B3", "ó").Replace("%C5%9B", "ś").Replace("%C5%BA", "ź").Replace("%C5%BC", "ż").
                            Replace("%C4%84", "Ą").Replace("%C4%86", "Ć").Replace("%C4%98", "Ę").Replace("%C5%BB", "Ż").Replace("%C5%B9", "Ź").Replace("%C5%81", "Ł").Replace("%C3%93", "Ó").Replace("%C5%83", "Ń").Replace("%C5%9A", "Ś").Replace("%E2%80%93", "-").Replace("%2C", ",");
                String DescriptionP = (tokens[2].Split(new[] { "=" }, StringSplitOptions.None))[1].
                            Replace("+", " ").Replace("%0D%0A", "<br>").Replace("%28", "(").Replace("%29", ")").Replace("%C4%85", "ą").Replace("%C4%87", "ć").Replace("%C4%99", "ę").Replace("%C5%82", "ł").Replace("%C5%84", "ń").Replace("%C3%B3", "ó").Replace("%C5%9B", "ś").Replace("%C5%BA", "ź").Replace("%C5%BC", "ż").
                            Replace("%C4%84", "Ą").Replace("%C4%86", "Ć").Replace("%C4%98", "Ę").Replace("%C5%BB", "Ż").Replace("%C5%B9", "Ź").Replace("%C5%81", "Ł").Replace("%C3%93", "Ó").Replace("%C5%83", "Ń").Replace("%C5%9A", "Ś").Replace("%E2%80%93", "-").Replace("%2C", ",");
                String SportP = (tokens[3].Split(new[] { "=" }, StringSplitOptions.None))[1].
                            Replace("+", " ").Replace("%0D%0A", "<br>").Replace("%28", "(").Replace("%29", ")").Replace("%C4%85", "ą").Replace("%C4%87", "ć").Replace("%C4%99", "ę").Replace("%C5%82", "ł").Replace("%C5%84", "ń").Replace("%C3%B3", "ó").Replace("%C5%9B", "ś").Replace("%C5%BA", "ź").Replace("%C5%BC", "ż").
                            Replace("%C4%84", "Ą").Replace("%C4%86", "Ć").Replace("%C4%98", "Ę").Replace("%C5%BB", "Ż").Replace("%C5%B9", "Ź").Replace("%C5%81", "Ł").Replace("%C3%93", "Ó").Replace("%C5%83", "Ń").Replace("%C5%9A", "Ś").Replace("%E2%80%93", "-").Replace("%2C",",");
                SQLiteDatabase DB = new SQLiteDatabase(Assembly.GetEntryAssembly().Location, "svs.db");
                DB.ExecuteNonQuery("INSERT INTO ParticipantInfo VALUES(NULL,'" + NameP + "', '" + SurnameP + "', '" + DescriptionP + "', '" + SportP + "', 1)");
                return View["add.html", new {msg=successMsg}];
            };         
        }
        const String successMsg = @"
            <div class='alert alert-success' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
                <strong>Well done! </strong>
                Your favourite athlete was add ;)
            </div>
            ";
    }
}