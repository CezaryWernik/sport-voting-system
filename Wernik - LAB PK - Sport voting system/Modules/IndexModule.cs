﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Wernik___LAB_PK___Sport_voting_system.Models
{
    public class IndexModule : Nancy.NancyModule
    {
        public IndexModule()
        {
            Get["/"] = parameter => {
                Console.WriteLine("Get[/]");
                var athletes = getPrincipants();
                var model = new { msg = "", athletes };
                return View["index.html", model];
            };

            Get["/xml"] = parameter => {
                Console.WriteLine("Get[/xml]");
                var athletes = getPrincipants();
                var model = new { athletes };
                return View["xml.html", model];
            };

            Post["/"] = parameter => {
                var body = this.Request.Body;
                int length = (int)body.Length;
                byte[] data = new byte[length];
                body.Read(data, 0, length);
                Console.WriteLine("Post[/] :: " + System.Text.Encoding.Default.GetString(data));
                var athletes = getPrincipants();
                if (data.Length > 0)
                {
                    string[] tokens = System.Text.Encoding.Default.GetString(data).Split(new[] { "&" }, StringSplitOptions.None);
                    String cmd = (tokens[0].Split(new[] { "=" }, StringSplitOptions.None))[1];
                    String id = (tokens[1].Split(new[] { "=" }, StringSplitOptions.None))[1];
                    switch (cmd)
                    {
                        case "up":
                            {
                                SQLiteDatabase DB = new SQLiteDatabase(Assembly.GetEntryAssembly().Location, "svs.db");
                                DB.ExecuteNonQuery(@"INSERT INTO Votes VALUES(NULL,'" + id + "', 1)");
                                athletes = getPrincipants();
                                var modelUp = new { msg = voteUp, athletes };
                                return View["index.html", modelUp];
                            }
                            break;
                        case "down":
                            {
                                SQLiteDatabase DB = new SQLiteDatabase(Assembly.GetEntryAssembly().Location, "svs.db");
                                DB.ExecuteNonQuery(@"INSERT INTO Votes VALUES(NULL,'" + id + "', 0)");
                                athletes = getPrincipants();
                                var modelDown = new { msg = voteDown, athletes };
                                return View["index.html", modelDown];
                            }
                            break;
                        case "preview":
                            {
                                var athlete = getPrincipant(id);
                                var ath = athlete.ElementAt(0);

                                var modelPreview = new { 
                                    msg = nullMsg,
                                    Name = ath.Name,
                                    Surname = ath.Surname,
                                    Description = ath.Description,
                                    Sport = ath.Sport,
                                    Points = ath.Points,
                                    PointsP = ath.PointsP,
                                    PointsN = ath.PointsN
                                };
                                return View["preview.html", modelPreview];
                            }
                            break;
                        default:
                            {
                            }
                            break;
                    }
                }
                var model = new { msg = nullMsg, athletes };
                return View["index.html", model];
            };
        }

        List<Participant> getPrincipants() {
            SQLiteDatabase DB = new SQLiteDatabase(Assembly.GetEntryAssembly().Location, "svs.db");
            DataTable dataDB = DB.GetDataTable("SELECT Id, Name, Surname, Description, Sport, Status FROM ParticipantInfo WHERE Status==1");
            var athlete = Enumerable
                .Range(1, dataDB.Rows.Count)
                .Select(i => new Participant());

            int k = 0;
            int xp = 0, xn = 0;
            List<Participant> ath = athlete.ToList<Participant>();
            foreach (DataRow row in dataDB.Rows)
            {
                ath.ElementAt(k).Id = (int)row.Field<Int64>(0);
                ath.ElementAt(k).Name = row.Field<string>(1);
                ath.ElementAt(k).Surname = row.Field<string>(2);
                ath.ElementAt(k).Description = row.Field<string>(3);
                ath.ElementAt(k).Sport = row.Field<string>(4);
                Int32.TryParse(DB.ExecuteScalar("SELECT count(*) FROM Votes WHERE ParticipantId=='" + (int)row.Field<Int64>(0) + "' and Status=='1'"), out xp);
                Int32.TryParse(DB.ExecuteScalar("SELECT count(*) FROM Votes WHERE ParticipantId=='" + (int)row.Field<Int64>(0) + "' and Status=='0'"), out xn);
                ath.ElementAt(k).Points = xp - xn;
                ath.ElementAt(k).PointsP = xp;
                ath.ElementAt(k).PointsN = xn;
                k++;
            }
            List<Participant> sath = ath.OrderByDescending(o => o.Points).ToList();
            return sath;
        }

        List<Participant> getPrincipant(String id)
        {
            SQLiteDatabase DB = new SQLiteDatabase(Assembly.GetEntryAssembly().Location, "svs.db");
            DataTable dataDB = DB.GetDataTable("SELECT Id, Name, Surname, Description, Sport FROM ParticipantInfo WHERE Id==" + id + "");
            var athletes = Enumerable
                .Range(1, dataDB.Rows.Count)
                .Select(i => new Participant());

            int k = 0;
            int xp = 0, xn = 0;
            List<Participant> ath = athletes.ToList<Participant>();
            foreach (DataRow row in dataDB.Rows)
            {
                ath.ElementAt(k).Id = (int)row.Field<Int64>(0);
                ath.ElementAt(k).Name = row.Field<string>(1);
                ath.ElementAt(k).Surname = row.Field<string>(2);
                ath.ElementAt(k).Description = row.Field<string>(3); 
                ath.ElementAt(k).Sport = row.Field<string>(4);
                Int32.TryParse(DB.ExecuteScalar("SELECT count(*) FROM Votes WHERE ParticipantId=='" + (int)row.Field<Int64>(0) + "' and Status=='1'"), out xp);
                Int32.TryParse(DB.ExecuteScalar("SELECT count(*) FROM Votes WHERE ParticipantId=='" + (int)row.Field<Int64>(0) + "' and Status=='0'"), out xn);
                ath.ElementAt(k).Points = xp - xn;
                ath.ElementAt(k).PointsP = xp;
                ath.ElementAt(k).PointsN = xn;
                k++;
            }
            return ath;
        }

        const String voteUp = @"
            <div class='alert alert-success' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
                <strong>Well done! </strong>
                Your favourite athlete was vote positive ;)
            </div>
            ";

        const String voteDown = @"
            <div class='alert alert-danger' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
                <strong>Ok! </strong>
                Your chosen athlete was vote negative;)
            </div>
            ";
        const String nullMsg = "";
    }
}