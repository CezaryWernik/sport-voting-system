﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wernik___LAB_PK___Sport_voting_system.Models
{
    class Participant
    {
        public Participant() {}
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Description { get; set; }
        public string Sport { get; set; }
        public bool Status { get; set; }
        public int Points { get; set; }
        public int PointsP { get; set; }
        public int PointsN { get; set; }
    }
}
