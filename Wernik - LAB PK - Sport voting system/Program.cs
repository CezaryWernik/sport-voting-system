﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;
using System.Data.SQLite;

namespace Wernik___LAB_PK___Sport_voting_system
{
    class Program
    {

        const string domain = "http://127.0.0.1:80";
        static void Main(string[] args)
        {
            var nancyHost = new Nancy.Hosting.Self.NancyHost(new Uri(domain)); // create a new self-host server
            nancyHost.Start(); // start
            Console.WriteLine("Service listening on " + domain);

            SQLiteDatabase DB = new SQLiteDatabase(Assembly.GetEntryAssembly().Location, "svs.db");
            DB.ExecuteNonQuery(
                @"create table if not exists [ParticipantInfo] (
                [Id] INTEGER PRIMARY KEY ASC,
                [Name] varchar(40) ,
                [Surname] varchar(40) ,
                [Description] varchar(255),
                [Sport] varchar(40),
                [Status] bool)"
                );
            DB.ExecuteNonQuery(
                @"create table if not exists [Votes] (
                [Id] INTEGER PRIMARY KEY ASC,
                [ParticipantId] integer,
                [Status] bool,
                FOREIGN KEY(ParticipantId) REFERENCES Participant(Id))"
                );

            Console.ReadLine(); // stop with an <Enter key press>
            nancyHost.Stop();
        }
    }

    // utterly basic configuration of the Nancy server. Other configuration not researched.
    public class Bootstrapper : Nancy.DefaultNancyBootstrapper
    {
        protected virtual Nancy.Bootstrapper.NancyInternalConfiguration InternalConfiguration
        {
            get
            {
                return Nancy.Bootstrapper.NancyInternalConfiguration.Default;
            }
        }
    }
}





